/*
 * Cada clase debe de estar dentro de un package.
 */
package ejerciciosClase;

import java.util.Scanner;
//La clase no se puede llamar comenzando en min�scula.
public class EjercicioRefactorizar1 {

	/*
	 * Los nombres de constantes deben de ir en may�sculas, habr�a que poner CAD en lugar de cad.
	 */
	final static String CAD = "Bienvenido al programa";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Clase para refactorizar
		 * Codificacion UTF-8
		 * Paquete: ra3.refactorizacion
		 * Se deben comentar todas las refactorizaciones realizadas,
		 * mediante comentarios de una linea o de bloque.
		 */

		/*
		 * Tanto el valor de "a" como el valor de "b" deber�an de ser inicializadas
		 * al principio a la vez que se crean ambos int.
		 */
				int a, b;
				String cadena1, cadena2;
				/*
				 * El Scanner no se puede crear con el nombre de una sola letra.
				 */
				Scanner lector = new Scanner(System.in);
				
				//Dentro del Syso, cad debe de ir en may�sculas porque anteriormente estaba mal creado.
				System.out.println(CAD);
				System.out.println("Introduce tu dni");
				cadena1 = lector.nextLine();
				System.out.println("Introduce tu nombre");
				cadena2 = lector.nextLine();
				
				/*
				 * Se deben de poner espacios para poder leerlo sin problemas al leer c�digo.
				 * Ademas al darle un valor a la letra "a" se pone en una l�nea, y cuando se le da el 
				 * valor a la letra "b" se escribe en la l�nea siguiente.
				 */
				a=7; b=16;
				int numeroc = 25;
				/*
				 * Se deben de poner espacios para poder leerlo sin problemas al leer c�digo,
				 * adem�s la llave que se abre justo despues de crear el if no debe ir en la 
				 * linea siguiente, sino que debe de ir justo seguida al par�ntesis del propio if.
				 */
				
				if(a > b || numeroc % 5 != 0 && (numeroc * 3 - 1) > b / numeroc)
				{
					System.out.println("Se cumple la condici�n");
				}
				
				//Se deben de poner espacios para poder leerlo sin problemas al leer c�digo.
				numeroc = a + b * numeroc + b / a;
				
				String array[] = new String[7];
				//Los dias de la semana deben de ir en bloque.
				//Los corchetes tienen que estar seguidos del String y no del array.
				String[] arrayDiasSemana = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
				
				recorrer_array(array);
				//Se debe de cerrar el Scanner.
				lector.close();
			}
	
			/*
			 * La llave que va seguida al "static void recorrer_array(String vectordestrings[])"
			 * debe de ir en esa misma l�nea seguida a este m�todo, no en la l�nea siguiente.
			 */
			static void recorrer_array(String vectordestrings[]){
				/*Se deben de poner espacios para poder leerlo sin problemas al leer c�digo,
				 * adem�s la llave que se abre justo despues de crear el for no debe ir en la 
				 * linea siguiente, sino que debe de ir justo seguida al par�ntesis del propio for.
				 */
				for(int dia = 0 ; dia < 7 ; dia++){
					System.out.println("El dia de la semana en el que te encuentras ["+(dia+1)+"-7] es el dia: "+vectordestrings[dia]);
				}
			}
			
		/*
		 * Hay que cerrar el Scanner introduciendo: "c.close();"
		 */
	}


