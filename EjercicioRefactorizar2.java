package ejerciciosClase;
/* 
 * Se debe de crear la clase dentro de un paquete.
 */

import java.io.*;
import java.util.*;

public class EjercicioRefactorizar2 {

	static Scanner lector;
	//No se debe de crer un Scanner con un solo caracter como nombre.
	public static void main(String[] args){//La llave que habre el public static void main (String[] args) debe de 
											//ir en la misma l�nea.
		
		lector = new Scanner(System.in);
		
		int n, cantidad_maxima_alumnos;
		
		/*Una vez que creas la variable int, se deberia de inicializar  en esa misma l�nea, en
		 * lugar de volver a escribir 2 lineas mas abajo el nombre de la variable para darle un valor.
		 */
		cantidad_maxima_alumnos = 10;
		int arrays[] = new int[10];
		/*
		 * Se deben de dejar espacios dentro del par�tesis del for para que en el momento
		 * de leer codigo se pueda leer correctamente.
		 */
		for(n = 0 ; n < 10 ; n++)
		{
			System.out.println("Introduce nota media de alumno");
			arrays[n] = lector.nextInt();
			/*
			 * En el momento de guardar el valor que se introduce por consola, al ser un valor int
			 * se debe de escribir el nombre del Scanner.nextInt();
			 */
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arrays));
		//Falta un parentesis para cerrar el syso.
		
		lector.close();
	}
	static double recorrer_array(int vector[])
	{
		double c = 0;
		/*Se deben dejar espacios dentro del parentesis del for para a la hora de leer c�digo
		 * poder leerlo sin ning�n problema.
		 */
		for(int a = 0 ; a < 10 ; a++) 
		{
			/*
			 * Se deben de dejar espacios, en lugar de poner todo el c�digo seguido para poder leer
			 * el c�digo sin problemas o confusiones.
			 */
			c = c + vector[a];
		}
		//Se deben dejar espacios en el return para leer el codigo correctamente.
		return c/10;

	}

}
