package ejercicio03;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in);
		
		menu(lector);
		
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = lector.nextInt();
		System.out.println("Elige tipo de password: ");
		int opcion = lector.nextInt();
		
		String password = "";
		switch (opcion) {
		case 1:
			password = generaLetras(longitud, password);
			break;
		case 2:
			password = generaNumeros(longitud, password);
			break;
		case 3:
			password = generaLetrasYNumeros(longitud, password);
			break;
		case 4:
			password = generaLetrasNumerosYCaracteresEspeciales(longitud, password);
			break;
		}

		System.out.println(password);
		lector.close();
	}

	private static String generaLetrasNumerosYCaracteresEspeciales(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 3);
			if (n == 1) {
				char letra4;
				letra4 = letraAleatoria();
				password += letra4;
			} else if (n == 2) {
				char caracter4;
				caracter4 = (char) ((Math.random() * 15) + 33);
				password += caracter4;
			} else {
				int numero4;
				numero4 = (int) (Math.random() * 10);
				password += numero4;
			}
		}
		return password;
	}

	private static String generaLetrasYNumeros(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 2);
			if (n == 1) {
				char letra3;
				letra3 = letraAleatoria();
				password += letra3;
			} else {
				char caracter3;
				caracter3 = (char) ((Math.random() * 15) + 33);
				password += caracter3;
			}
		}
		return password;
	}

	private static String generaNumeros(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int numero2;
			numero2 = (int) (Math.random() * 10);
			password += numero2;
		}
		return password;
	}

	private static String generaLetras(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			password += letraAleatoria();
		}
		return password;
	}

	private static char letraAleatoria() {
		return (char) ((Math.random() * 26) + 65);
	}
	
	public static void menu(Scanner lector) {
		
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		
	}
	
	
	

}
