package ejercicio04;


import java.io.File;
import java.util.Scanner;

public class Principal {
	
		private final static byte NUM_PALABRAS = 20;
		private final static byte FALLOS = 7;
		private static String[] palabras = new String[NUM_PALABRAS];

		public static void main(String[] args) {

			String palabraSecreta, ruta = "src\\palabras.txt";

			File fich = new File(ruta);
			Scanner inputFichero = null;

			leerFichero(fich, inputFichero);

			Scanner input = new Scanner(System.in);

			palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];

			char[][] caracteresPalabra = new char[2][];
			caracteresPalabra[0] = palabraSecreta.toCharArray();
			caracteresPalabra[1] = new char[caracteresPalabra[0].length];

			String caracteresElegidos = "";
			int fallos;
			boolean acertado;
			System.out.println("Acierta la palabra");
			do {

				System.out.println("####################################");

				mostrarPalabra(caracteresPalabra);
				System.out.println();

				System.out.println("Introduce una letra o acierta la palabra");
				System.out.println("Caracteres Elegidos: " + caracteresElegidos);
				caracteresElegidos += input.nextLine().toUpperCase();
				fallos = 0;

				fallos = comprobarCaracter(caracteresPalabra, caracteresElegidos, fallos);

				mostrarAhorcado(fallos);

				acertado = juegoTerminado(palabraSecreta, caracteresPalabra, fallos);

			} while (!acertado && fallos < FALLOS);

			input.close();
		}

		private static boolean juegoTerminado(String palabraSecreta, char[][] caracteresPalabra, int fallos) {
			boolean acertado;
			if (fallos >= FALLOS) {
				System.out.println("Has perdido: " + palabraSecreta);
			}
			acertado = true;
			for (int i = 0; i < caracteresPalabra[1].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					acertado = false;
					break;
				}
			}
			if (acertado)
				System.out.println("Has Acertado ");
			return acertado;
		}

		private static void mostrarAhorcado(int fallos) {
			switch (fallos) {
			case 1:

				System.out.println("     ___");
				break;
			case 2:

				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 3:
				System.out.println("  ____ ");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 4:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 5:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 6:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println("     ___");
				break;
			case 7:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println(" A   ___");
				break;
			}
		}

		private static int comprobarCaracter(char[][] caracteresPalabra, String caracteresElegidos, int fallos) {
			boolean encontrado;
			for (int j = 0; j < caracteresElegidos.length(); j++) {
				encontrado = false;
				for (int i = 0; i < caracteresPalabra[0].length; i++) {
					if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
						caracteresPalabra[1][i] = '1';
						encontrado = true;
					}
				}
				if (!encontrado)
					fallos++;
			}
			return fallos;
		}

		private static void mostrarPalabra(char[][] caracteresPalabra) {
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					System.out.print(" -");
				} else {
					System.out.print(" " + caracteresPalabra[0][i]);
				}
			}
		}

		private static void leerFichero(File fich, Scanner inputFichero) {
			try {
				inputFichero = new Scanner(fich);
				for (int i = 0; i < NUM_PALABRAS; i++) {
					palabras[i] = inputFichero.nextLine();
				}
			} catch (Exception e) {
				System.out.println("Error al abrir fichero: " + e.getMessage());
			} finally {
				if (fich != null && inputFichero != null)
					inputFichero.close();
			}
			System.out.println("");
		}

	}
